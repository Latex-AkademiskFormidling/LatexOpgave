\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {danish}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Indledning}{1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Problemformulering}{2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Hvad er et Football Manager-spil?}{3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.0.1}Generelt}{3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.0.2}Fifa Manager}{3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.0.3}Football Manager}{3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.0.4}Virtual Manager}{3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Analyse Produkter}{4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Markedsanalyse}{4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.1}Konkurrenter}{4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.2}Kunder}{4}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Kvalitative Unders\IeC {\o }gelser}{4}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Kvantitative Unders\IeC {\o }gelser}{5}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Persona}{5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Use Case Diagram}{5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}Use Case Tekst}{5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.4}Klassediagram}{5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.5}H\IeC {\ae }ndelsestabel}{6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.6}ER-modellen}{6}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Konklusion}{7}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Use Case}{7}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Persona}{7}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{ER-modellen}{7}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{SWOT}{7}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Klassediagram og h\IeC {\ae }ndelsestabel}{7}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {A}Bilag}{9}
